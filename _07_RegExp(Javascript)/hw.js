/*При выполнении всех заданий использовать регулярные выражения.

1. Напишите функцию isUpperCaseFirstLetter, которая проверяет входную строку на наличие
первой прописной (большой буквы).
Возвращает true - если первая буква в слове прописна, иначе - false.

2. Напишите функцию isValidEmail, которая проверяет является ли входная строка валидным email адресом.
    Возвращает true - если email валидный, иначе - false.

3. Напишите функцию trimString, которая работает по аналогии со строковым методом trim,
    т.е. удаляет начальные и конечные пробелы, а также удаляет повторяющиеся пробелы внутри строки.
    Возвращает измененную строку.

4. Напишите функцию thousandsSeparators для отделения тысяч при печати целой части числа.
    Функция принимает число (number), а возвращает строку.

    Пример:
console.log(thousandsSeparators(1000));
"1,000"

console.log(thousandsSeparators(10000.23));
"10,000.23"
*/
function isUpperCaseFirstLetter(str) {
    console.log(str[0]);
    if (str.search(/[A-ZА-Я]/) == 0) {
        return true;
    }
    return false;
}
// isUpperCaseFirstLetter("Lorem");      test
// isUpperCaseFirstLetter("lorem");      test



function isValidEmail(str) {

    let emailRegExp = new RegExp(/[\w]+@[\w]+\.[\w]+/g);
    return emailRegExp.test(str);
}

function trimString(str) {

var newStr = str.replace(/^\s+|\s+$/g,"");

newStr = newStr.replace(/\s+/g, " ");

    return str = newStr;
}

trimString("   Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.   ");

function thousandsSeparators(num) {
    num = num.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(num))
        num = num.replace(pattern, "$1,$2");
    return num;
}
 // formatCurrency(1000);
