/*
 Напишите JavaScript функию для клонирования массива.

 Пример:
 console.log(array_clone([1, 2, 4, 0]));
 console.log(array_clone([1, 2, [4, 0]]));

 Ожидаемые результаты:
 [1, 2, 4, 0]
 [1, 2, [4, 0]]

 2. Написать функцию summ_kv для нахождения суммы квадратов чисел массива.

 Пример:
 console.log(summ_kv(1,2,3,4));

 Ожидаемые результаты:
 30

 3.  Написать функцию unique, которая возвращает уникальные в массиве значения.

 Пример:

 console.log(unique([7, 9, 0, -2]));
 console.log(unique([7, 7, 0, -2]));
 console.log(unique([7, 9, 9, -2]));
 console.log(unique([1, 1, 1, 1]));

 Ожидаемые результаты:
 [7, 9, 0, -2]
 [9, 0, -2]
 [7, -2]
 []

 Видео Объект, Массивы, Объект Math - https://youtu.be/6g45IsIQtaY
*/

function array_clone(arr) {
    let Object = {};
    for (let k in arr) {
        Object[k] = arr[k];
    }
    return Object;
}

console.log(array_clone([1, 2, 4, 0]));
console.log(array_clone([1, 2, [4, 0]]));

function summ_kv() {
    let summSqad = 0;
    for (let i = 0; i < arguments.length; i++) {
        summSqad += arguments[i] * arguments[i]; // summ sqad
    }
    return summSqad;
}
console.log(summ_kv(1, 2, 3, 4));
// result 30

function unique(arr) {
    i = arr.length;
    while (i--) {
        if (arr[i] == arr[i-1]) {
            arr.splice(i, 1);
            delete arr[i-1];
        }
    }
    return arr;
}
console.log(unique([7, 9, 0, -2]));
console.log(unique([7, 7, 0, -2]));
console.log(unique([7, 9, 9, -2]));
console.log(unique([1, 1, 1, 1]));

//  Result:
//      [7, 9, 0, -2]
//      [9, 0, -2]
//      [7, -2]
//      []
