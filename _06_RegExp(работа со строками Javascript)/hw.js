/*1. Написать функцию isPalindrom(str), которая принимает строку и возвращает
true если строка является палиндромом и false - если нет
Например: строка madam - палиндром, а mama - нет

2. Напишите функцию ucFirst(str), которая возвращает строку str с заглавным первым символом каждого слова, например:

ucFirst("вася") == "Вася";
ucFirst("") == ""; // нет ошибок при пустой строке
P.S. В JavaScript нет встроенного метода для этого. Создайте функцию, используя toUpperCase() и charAt().

3. Напишите функцию checkSpam(str), которая возвращает true, если строка str содержит „viagra“ или „XXX“, а иначе false.

    Функция должна быть нечувствительна к регистру:

    checkSpam('buy ViAgRA now') == true
checkSpam('free xxxxx') == true
checkSpam("innocent rabbit") == false

4. Есть стоимость в виде строки: "$120". То есть, первым идёт знак валюты, а затем – число.

    Создайте функцию extractCurrencyValue(str), которая будет из такой строки выделять число-значение, в данном случае 120.

5. Напишите JavaScript функцию которая возвращает переданную строку в алфавитном порядке.
    Пример строки : 'webmaster'
Пример результата: 'abeemrstw'
*/

function isPalindrom(str) {
    for(let i = 0; i < str.length/2; i++){
        if(str[i] !== str[str.length - 1 -i]){
            return false;
        }
    }
    return true;
}

function ucFirst(str) {
    let newStr = "";
    let space = false;
    let regExp = new RegExp(/[A-Za-zА-Яа-я]/);
    for(let i = 0; i < str.length; i++){
        let letter = str[i];
        if(i===0 && regExp.test(letter)){
            letter = letter.toUpperCase();
            space = false;
        } else if(/\s/.test(letter)){
            space = true;
        } else if(space && regExp.test(letter)){
            letter = letter.toUpperCase();
            space = false;
        }
        newStr+=letter;
    }
    return newStr;
}

function checkSpam(str) {   //содержит "viagra XXX"

    var lowerStr = str.toLowerCase();

    return !!(~lowerStr.indexOf('viagra') || ~lowerStr.indexOf('xxx'));
}
checkSpam(" what are you doing?"); // проверка
checkSpam(" Hello Viagra");        // проверка
checkSpam(" Hello xxX");           // проверка

function extractCurrencyValue(str) {
    var newstr = "";
    for (var i = 0; i <= str.length-1; i++) {
        if (!(str[i] < "0" || str[i] > "9")) {
            newstr += str[i]
        }
    }
    return +newstr
}

function toAlphaBet(str) {
    str = str.split("");
    str = str.sort();
    str = str.join("");
    return str;
}
formatCurrency(1000);
